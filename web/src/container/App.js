import React, { Component } from "react";
import { hterm, lib } from "hterm-umdjs";

import { Terminal, TabGroup } from "../components";

import "./App.css";

var NUM_TERMINAL = 0;

hterm.defaultStorage = new lib.Storage.Local();
hterm.defaultStorage.clear();

let newTerminal = _ => ({
  id: +(new Date()),
  number: NUM_TERMINAL++,
  term: new hterm.Terminal(),
  socket: new WebSocket(`ws://${window.location.hostname}:${window.location.port}/pty`)
})

class App extends Component {
  state = {
    terminals: [],
    selectedTabKey: 0
  }
  componentWillMount() {
    if (this.state.terminals.length === 0) return;
    // eslint-disable-next-line
    this.state.selectedTabKey = this.state.terminals[0].id
  }
  onTabClose = index => {
    let { terminals, selectedTabKey } = this.state;
    let newIndex = -1, selectedIndex = -1;
    for (let i = 0; i < terminals.length; i++) {
      if (terminals[i].id === selectedTabKey) {
        selectedIndex = i;
        break;
      }
    }
    if (index === selectedIndex) {
      if (index !== 0) newIndex = index - 1;
      else newIndex = 0;
    } else {
      newIndex = selectedIndex - 1;
    }
    try {
      terminals[index].socket.send("5");
      terminals[index].socket.close();
    } catch (error) {
      console.error(error);
    }
    terminals.splice(index, 1);
    if (newIndex >= 0 && terminals.length > 0) {
      selectedTabKey = terminals[newIndex].id;
    }
    this.setState({ terminals, selectedTabKey });
  }
  onAddClick = _ => {
    let { terminals, selectedTabKey } = this.state;
    let empty = terminals.length === 0;
    terminals.push(newTerminal());
    if (empty) {
      selectedTabKey = terminals[0].id;
    }
    this.setState({ terminals, selectedTabKey });
  }
  onTitleClick = index => {
    let { selectedTabKey, terminals } = this.state;
    selectedTabKey = terminals[index].id;
    this.setState({ selectedTabKey });
  }
  render() {
    let { terminals, selectedTabKey } = this.state;
    let tabTitles = terminals.map(t => ({
      name: `Terminal ${t.number}`,
      selected: selectedTabKey === t.id,
    }));
    return (
      <div className="view-extension">
        <TabGroup
          tabTitles={tabTitles}
          onTabClose={this.onTabClose}
          onAddClick={this.onAddClick}
          onTitleClick={this.onTitleClick}
          selectedKeyTest={child => selectedTabKey === +child.key}>
          {terminals.map(
            t => (
              <Terminal key={t.id} source={t.term} socket={t.socket} />
            )
          )}
        </TabGroup>
      </div>
    );
  }
}

export default App;


// TODO: auto remove tab when remote terminal is exited
// TODO: code editor? file browser? file download/upload?
// TODO: system management... reboot/shutdown/common system settings
// TODO: popout windows
// TODO: confirm popup when on tab close