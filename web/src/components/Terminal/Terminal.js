import React, { Component } from 'react';

class Terminal extends Component {
    componentWillUnmount() {
        this.terminal.uninstallKeyboard();
    }
    componentDidMount() {
        let { source, socket } = this.props;
        let container = this.refs.root;
        this.socket = socket;
        this.terminal = source;

        let setup = _ => {
            this.terminal.getPrefs().set("send-encoding", "raw");
            this.terminal.getPrefs().set('environment', { TERM: 'xterm' })
            this.terminal.onTerminalReady = _ => {
                var io = this.terminal.io.push();
                io.onVTKeystroke = (str) => {
                    this.socket.send("0" + str);
                };
                io.sendString = io.onVTKeystroke;
                io.onTerminalResize = (columns, rows) => {
                    this.socket.send(
                        "2" + JSON.stringify(
                            {
                                columns: columns,
                                rows: rows,
                            }
                        )
                    )
                };
                this.terminal.installKeyboard();
            };
            this.terminal.decorate(container);
        }

        if (this.socket.readyState === 1) {
            this.terminal.decorate(container);
            this.terminal.installKeyboard();
        } else {
            this.socket.onopen = setup;
        }
        this.socket.onmessage = (event) => {
            let data = event.data.slice(1);
            this.terminal.io.writeUTF8(window.atob(data));
        }
    }
    render() {
        return (
            <div className="view-extension Terminal" ref="root"></div>
        );
    }
}

export default Terminal;
