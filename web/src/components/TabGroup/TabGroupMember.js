import React, { Component } from "react";
import classnames from "classnames";

class TabGroupMember extends Component {
    render() {
        let { show, children } = this.props;
        let classNames = classnames({
            "TabGroup__member": true,
            "TabGroup__member--show": show
        })
        return (
            <div className={classNames}>
                {children}
            </div>
        );
    }
}

export default TabGroupMember;
