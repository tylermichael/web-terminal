import React, { Component } from "react";

import { ToolBar } from "../"
import TabGroupMember from "./TabGroupMember";
import "./TabGroup.css";

class TabGroup extends Component {
    state = {
        selectedIndex: 0
    }
    onTitleClick = (index) => {
        this.props.onTitleClick(index);
    }
    onCloseClick = (index, event) => {
        event.stopPropagation();
        this.props.onTabClose(index);
    }
    onAddClick = _ => {
        this.props.onAddClick();
    }
    render() {
        let { tabTitles, selectedKeyTest, children } = this.props;
        return (
            <div className="view-extension">
                <div className="TabGroup">
                    {React.Children.map(children, (child, index) => {
                        return <TabGroupMember
                            key={index}
                            show={selectedKeyTest(child)}
                            >{child}</TabGroupMember>
                    })}
                </div>
                <ToolBar
                    onTitleClick={this.onTitleClick}
                    onCloseClick={this.onCloseClick}
                    onAddClick={this.onAddClick}
                    tabTitles={tabTitles} />
            </div>
        );
    }
}

export default TabGroup;
