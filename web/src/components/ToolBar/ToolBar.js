import React, { Component } from "react";
import classnames from "classnames";

import "./ToolBar.css";

class ToolBar extends Component {
    render() {
        let {
            tabTitles,
            onTitleClick,
            onCloseClick,
            onAddClick
        } = this.props;
        return (
            <div className="ToolBar">
                {tabTitles.map((title, index) => {
                    let cn = classnames({
                        "ToolBar__title": true,
                        "ToolBar__title--selected": title.selected,
                    });
                    return <div key={index}
                        onClick={onTitleClick.bind(null, index)}
                        className={cn}
                        >{title.name}
                        <span
                            className={"ToolBar__title__close"}
                            onClick={onCloseClick.bind(null, index)}>×</span></div>
                })}
                <div className="ToolBar__button" onClick={onAddClick}>+</div>
            </div>
        );
    }
}

export default ToolBar;
