# web-terminal

web-terminal allows you to open up terminal sessions on your server with nothing more than your browser

![example image](docs/example-1.png)