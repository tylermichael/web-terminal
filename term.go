package main

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"os"
	"os/exec"
	"syscall"
	"unsafe"

	"github.com/gorilla/websocket"

	"sync"

	"net/http"

	"github.com/kr/pty"
)

// Quite alot of this code was lifted from/heavily influenced by https://github.com/yudai/gotty

// Term is the struct for a single connection to a pty
type Term struct {
	Request     *http.Request
	Connection  *websocket.Conn
	Pty         *os.File
	Cmd         *exec.Cmd
	WriteMutex  *sync.Mutex
	CloseSignal chan bool
}

type argResizeTerminal struct {
	Columns float64
	Rows    float64
}

const (
	Input          = '0'
	Ping           = '1'
	ResizeTerminal = '2'
)

const (
	Output         = '0'
	Pong           = '1'
	SetWindowTitle = '2'
	SetPreferences = '3'
	SetReconnect   = '4'
	Close          = '5'
)

// NewTerm returns a new Term struct
func NewTerm(connection *websocket.Conn, request *http.Request) *Term {
	return &Term{
		Connection:  connection,
		Request:     request,
		CloseSignal: make(chan bool, 1),
		WriteMutex:  &sync.Mutex{},
	}
}

// Spawn spawns a new pty with the given cmd and argv
func (t *Term) Spawn(cmd string, argv []string) error {
	t.Cmd = exec.Command(cmd, argv...)
	var err error
	t.Pty, err = pty.Start(t.Cmd)
	if err != nil {
		return err
	}
	log.Printf("Spawned pid %d\n", t.Cmd.Process.Pid)
	return nil
}

// Connect starts the Read and Write loops
func (t *Term) Connect() {
	defer t.CleanUp()
	go t.ReadSync()
	go t.WriteSync()
	<-t.CloseSignal
}

// CleanUp cleans up resouces for this session
func (t *Term) CleanUp() {
	t.Cmd.Process.Kill()
	log.Printf("killed pid %d\n", t.Cmd.Process.Pid)
}

// WriteSync writes messages from the Pty to the websocket
func (t *Term) WriteSync() {
	buf := make([]byte, 1024)
	for {
		size, err := t.Pty.Read(buf)
		if err != nil {
			log.Printf("Command exited for: %s", t.Request.RemoteAddr)
			break
		}
		safeMessage := base64.StdEncoding.EncodeToString([]byte(buf[:size]))
		if err = t.Write(append([]byte{Output}, []byte(safeMessage)...)); err != nil {
			log.Printf(err.Error())
			break
		}
	}
	t.CloseSignal <- true
}

// ReadSync reads messages from the websocket and writes them to the Pty
func (t *Term) ReadSync() {
	defer func() { t.CloseSignal <- true }()
	for {
		_, data, err := t.Connection.ReadMessage()
		if err != nil {
			log.Print(err.Error())
			return
		}
		if len(data) == 0 {
			log.Print("An error has occured")
			return
		}

		switch data[0] {
		case Input:
			_, err := t.Pty.Write(data[1:])
			if err != nil {
				return
			}

		case Ping:
			if err := t.Write([]byte{Pong}); err != nil {
				log.Print(err.Error())
				return
			}
		case ResizeTerminal:
			var args argResizeTerminal
			err = json.Unmarshal(data[1:], &args)
			if err != nil {
				log.Print("Malformed remote command")
				return
			}

			window := struct {
				row uint16
				col uint16
				x   uint16
				y   uint16
			}{
				uint16(args.Rows),
				uint16(args.Columns),
				0,
				0,
			}
			syscall.Syscall(
				syscall.SYS_IOCTL,
				t.Pty.Fd(),
				syscall.TIOCSWINSZ,
				uintptr(unsafe.Pointer(&window)),
			)
		case Close:
			return

		default:
			log.Print("Unknown message type")
			return
		}
	}
}

// Write writes data to the websocket
func (t *Term) Write(data []byte) error {
	t.WriteMutex.Lock()
	defer t.WriteMutex.Unlock()
	return t.Connection.WriteMessage(websocket.TextMessage, data)
}
